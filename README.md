# Couchdb transactions

Inspired by [txn](https://github.com/jhs/txn) this library loads, modifies and commits documents to couchdb. It also will retry failed requests.
The libray has zero dependencies but requires an instance of [nano](https://github.com/dscape/nano).

[![pipeline status](https://gitlab.com/revington/couchdb-transactions/badges/master/pipeline.svg)](https://gitlab.com/revington/couchdb-transactions/-/commits/master)
[![coverage report](https://gitlab.com/revington/couchdb-transactions/badges/master/coverage.svg)](https://gitlab.com/revington/couchdb-transactions/-/commits/master)


## Usage 

```javascript
const couchdbTxn = require('couchdb-transactions');
const db = require('nano')('http:....');

function sumTransaction(doc) {
    doc.sum = 1 + (doc.sum || 0);
}

let txn = couchdbTxn(db, sumTransaction);

let doc = {_id:'xyz'};

txn(doc, function (err, response){
    console.log('response', response);
);
```

## Contributing

Please respect the `.eslintrc` configuration.  
Also, you will need to create a `.env` file with the URL of your CouchDB server.  
See [.env.sample](.env.sample)

